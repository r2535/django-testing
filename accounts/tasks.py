
from dtesting.celery import app

from celery import shared_task # este decorador permite que la app este desacoplada al proyecto principal


@app.task
def successs_register_email_task():
    pass


@shared_task
def test_task_account():
    return 1 + 2


@shared_task
def add(x, y):
    return x + y


@shared_task
def mul(x, y):
    return x * y


@shared_task
def xsum(numbers):
    return sum(numbers)