import os

from celery import Celery


# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dtesting.settings')

app = Celery('dtesting')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
# link de documentacion para configuracion de celery en django https://docs.celeryq.dev/en/stable/django/first-steps-with-django.html
app.config_from_object('django.conf:settings', namespace='CELERY') #namespace es el prefijo que tendran todas las configraciones de celery en archivo settings

# Load task modules from all registered Django apps.
app.autodiscover_tasks() # esta linea indica que celery ejecutara todas las tareas encontradas dentro de las apps de mi proyecto


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')